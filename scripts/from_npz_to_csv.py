import numpy as np
import os
from pathlib import Path
import cv2

# Location where the experiments are stored
exps_folder = "../Velocity_Fields/"

# Loading the experiment folders
experiments = os.listdir(exps_folder)
plot_experiments = []
for experiment in experiments:
    if not os.path.isdir(os.path.join(exps_folder, experiment)):
        continue
    plot_experiments.append(experiment)

# Sorting experiments by flow rate, you may change for different analyses
plot_experiments = sorted(plot_experiments, key=lambda x: int(x.split("_kgh")[0].split("_")[-1]), reverse=True)

# Looping over the selected experiments
for experiment in plot_experiments:
    print("Processing/Plotting experiment %s!" % experiment)

    # Defining the file location
    plot_file = os.path.join(exps_folder, '%s/res/results_impeller.npz' % experiment)  # for impeller fields

    # Retrieving experiment name
    experiment_name = plot_file.split('/')[-3]

    # Create an output folder to store the plots
    output_folder_csv = Path('csv_files', experiment_name)
    output_folder_csv.mkdir(exist_ok=True, parents=True)

    # Loading the results
    RESULT_FILE = np.load(plot_file)

    # Reading the results
    X = RESULT_FILE['X']
    Y = RESULT_FILE['Y']
    U = RESULT_FILE['U']
    V = RESULT_FILE['V']
    Urms = RESULT_FILE['Urms']
    Vrms = RESULT_FILE['Vrms']
    UVrms = RESULT_FILE['UVrms']

    # Image mask
    img_mask = RESULT_FILE['img_mask']
    img_mask_true_resolution = cv2.resize(img_mask, (X.shape[1], X.shape[0]))
    geom_flag = img_mask > 10

    # Calculating the velocity magnitude
    U_mag = np.sqrt(U ** 2.0 + V ** 2.0)

    # Calculating the TKE
    TKE = 0.5 * (Urms ** 2.0 + Vrms ** 2.0)

    # Applying the image/pump mask into the fields
    img_mask_interpolated = cv2.resize(img_mask, (X.shape[1], X.shape[0]))
    geom_flag = img_mask_interpolated > 10
    U[geom_flag] = np.nan
    V[geom_flag] = np.nan
    U_mag[geom_flag] = np.nan
    TKE[geom_flag] = np.nan
    Urms[geom_flag] = np.nan
    Vrms[geom_flag] = np.nan
    UVrms[geom_flag] = np.nan

    # Storing each field in a separate .csv file
    np.savetxt(Path(output_folder_csv, 'geom_flag.csv'), geom_flag)
    np.savetxt(Path(output_folder_csv, 'X.csv'), X)
    np.savetxt(Path(output_folder_csv, 'Y.csv'), Y)
    np.savetxt(Path(output_folder_csv, 'U.csv'), U)
    np.savetxt(Path(output_folder_csv, 'V.csv'), V)
    np.savetxt(Path(output_folder_csv, 'U_mag.csv'), U_mag)
    np.savetxt(Path(output_folder_csv, 'TKE.csv'), TKE)
    np.savetxt(Path(output_folder_csv, 'Urms.csv'), Urms)
    np.savetxt(Path(output_folder_csv, 'Vrms.csv'), Vrms)
    np.savetxt(Path(output_folder_csv, 'UVrms.csv'), UVrms)