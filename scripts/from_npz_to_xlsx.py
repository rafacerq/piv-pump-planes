import numpy as np
import os
from pathlib import Path
import cv2
import pandas as pd

# Location where the experiments are stored
exps_folder = "../Velocity_Fields/"

# Loading the experiment folders
experiments = os.listdir(exps_folder)
plot_experiments = []
for experiment in experiments:
    if not os.path.isdir(os.path.join(exps_folder, experiment)):
        continue
    plot_experiments.append(experiment)

# Create an output folder to store the plots
output_folder_xlsx = Path('xlsx_files')
output_folder_xlsx.mkdir(exist_ok=True, parents=True)

# Sorting experiments by flow rate, you may change for different analyses
plot_experiments = sorted(plot_experiments, key=lambda x: int(x.split("_kgh")[0].split("_")[-1]), reverse=True)

# Looping over the selected experiments
for experiment in plot_experiments:
    print("Processing/Plotting experiment %s!" % experiment)

    # Defining the file location
    plot_file = os.path.join(exps_folder, '%s/res/results_impeller.npz' % experiment)  # for impeller fields

    # Retrieving experiment name
    experiment_name = plot_file.split('/')[-3]

    # Loading the results
    RESULT_FILE = np.load(plot_file)

    # Reading the results
    X = RESULT_FILE['X']
    Y = RESULT_FILE['Y']
    U = RESULT_FILE['U']
    V = RESULT_FILE['V']
    Urms = RESULT_FILE['Urms']
    Vrms = RESULT_FILE['Vrms']
    UVrms = RESULT_FILE['UVrms']

    # Image mask
    img_mask = RESULT_FILE['img_mask']
    img_mask_true_resolution = cv2.resize(img_mask, (X.shape[1], X.shape[0]))
    geom_flag = img_mask > 10

    # Calculating the velocity magnitude
    U_mag = np.sqrt(U ** 2.0 + V ** 2.0)

    # Calculating the TKE
    TKE = 0.5 * (Urms ** 2.0 + Vrms ** 2.0)

    # Applying the image/pump mask into the fields
    img_mask_interpolated = cv2.resize(img_mask, (X.shape[1], X.shape[0]))
    geom_flag = img_mask_interpolated > 10
    U[geom_flag] = np.nan
    V[geom_flag] = np.nan
    U_mag[geom_flag] = np.nan
    TKE[geom_flag] = np.nan
    Urms[geom_flag] = np.nan
    Vrms[geom_flag] = np.nan
    UVrms[geom_flag] = np.nan

    # Create MS Excel .xlsx file
    writer = pd.ExcelWriter(Path(output_folder_xlsx, f"{experiment_name}.xlsx"), engine="xlsxwriter")

    variables = [{'var': 'U ', 'val': U.T}, {'var': 'V ', 'val': V.T}, {'var': 'U_mag', 'val': U_mag.T},
                 {'var': 'Urms', 'val': Urms.T}, {'var': 'Vrms', 'val': Vrms.T},
                 {'var': 'UVrms', 'val': UVrms.T}, {'var': 'TKE', 'val': TKE.T},
                 {'var': 'X', 'val': X.T}, {'var': 'Y', 'val': Y.T},]

    # Create dataframes
    dfs = []
    for variable in variables:
        df = pd.DataFrame(variable['val'])
        dfs.append(df)

    # Create sheets
    for df, variable in zip(dfs, variables):
        df.to_excel(writer, sheet_name=variable['var'])
        workbook = writer.book
        worksheet = writer.sheets[variable['var']]

        # Create conditional coloring
        worksheet.conditional_format('B2:BO107', {"type": "3_color_scale"})

        # Set up default sheet zoom
        worksheet.set_zoom(10)

    # Close writer
    writer.close()